package fruits.service;

import fruits.dto.FruitDto;
import fruits.exception.FruitAlreadyExistException;
import fruits.exception.FruitNotFoundException;
import fruits.mock.data.MockData;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FruitService {
	private MockData mockData;

	public FruitService() {
		mockData = new MockData();
	}

	public List<FruitDto> getFruits() {
		return mockData.getFruits();
	}

	public FruitDto addFruit(FruitDto fruit) {
		if (!getFruit(fruit.getName()).isPresent()) {
			mockData.addFruit(fruit);
			return fruit;
		}

		throw new FruitAlreadyExistException(fruit.getName());
	}

	public void deleteFruit(String fruitName) {
		Optional<FruitDto> fruit = getFruit(fruitName);
		if (!fruit.isPresent()) {
			throw new FruitNotFoundException(fruitName);
		}
		mockData.deleteFruit(fruit.get());
	}

	private Optional<FruitDto> getFruit(String fruitName) {
		List<FruitDto> fruits = mockData.getFruits();

		return fruits.stream().filter(fruit -> fruit.getName().equals(fruitName)).findFirst();
	}
}
