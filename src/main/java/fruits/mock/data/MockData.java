package fruits.mock.data;

import fruits.dto.FruitDto;

import java.util.ArrayList;
import java.util.List;

public class MockData {
	private List<FruitDto> fruits;

	public MockData() {
		initFruits();
	}

	private void initFruits() {
		fruits = new ArrayList<>();

		fruits.add(new FruitDto("apples"));
		fruits.add(new FruitDto("pears"));
		fruits.add(new FruitDto("lemons"));
	}

	public List<FruitDto> getFruits() {
		return fruits;
	}

	public void addFruit(FruitDto fruit) {
		fruits.add(fruit);
	}

	public void deleteFruit(FruitDto fruit) {
		fruits.remove(fruit);
	}
}
