package fruits.dto;

public class FruitDto {
	String name;

	public FruitDto() {
	}

	public FruitDto(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
