package fruits.controller;

import fruits.dto.FruitDto;
import fruits.exception.FruitAlreadyExistException;
import fruits.exception.FruitNotFoundException;
import fruits.service.FruitService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sun.plugin2.util.PojoUtil.toJson;

@RunWith(MockitoJUnitRunner.class)
public class FruitControllerTest {
	@InjectMocks
	private FruitController fruitController;

	@Mock
	private FruitService fruitService;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(fruitController).build();
	}

	@Test
	public void getFruits_ExpectSuccess() throws Exception {
		List<FruitDto> fruits = asList(new FruitDto("fruit"));
		when(fruitService.getFruits()).thenReturn(fruits);
		this.mockMvc.perform(get("/fruits/getFruits"))
				.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void addFruits_ExpectSuccess() throws Exception {
		FruitDto fruit = new FruitDto("oranges");
		when(fruitService.addFruit(any())).thenReturn(fruit);

		this.mockMvc.perform(
				post("/fruits/addFruit")
						.contentType(MediaType.APPLICATION_JSON)
						.content(toJson(fruit)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value("oranges"));
	}

	@Test
	public void addFruits_ExpectException() throws Exception {
		when(fruitService.addFruit(any())).thenThrow(FruitAlreadyExistException.class);
		FruitDto fruit = new FruitDto("oranges");
		this.mockMvc.perform(
				post("/fruits/addFruit")
						.contentType(MediaType.APPLICATION_JSON)
						.content(toJson(fruit)))
				.andExpect(
						status().isBadRequest());
	}

	@Test
	public void deleteFruit_ExpectSuccess() throws Exception {
		this.mockMvc.perform(
				delete("/fruits/deleteFruit/{fruitName}", "oranges")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(
						status().isAccepted());
	}

	@Test
	public void deleteFruit_ExpectException() throws Exception {
		doThrow(FruitNotFoundException.class).when(fruitService).deleteFruit("oranges");
		this.mockMvc.perform(
				delete("/fruits/deleteFruit/{fruitName}", "oranges")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(
						status().isNotFound());
	}
}
