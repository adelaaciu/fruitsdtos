package fruits.service;

import fruits.dto.FruitDto;
import fruits.exception.FruitAlreadyExistException;
import fruits.exception.FruitNotFoundException;
import fruits.mock.data.MockData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class FruitServiceTest {
	private FruitService fruitService;
	private MockData mockData;

	@Before
	public void setUp() {
		fruitService = new FruitService();
		mockData = new MockData();
	}

	@Test
	public void getFruits() {
		List<FruitDto> fruits = mockData.getFruits();
		assertEquals(fruits.size(), fruitService.getFruits().size());
		for(int i = 0; i < fruits.size(); i++) {
			fruits.get(i).getName().equals(fruitService.getFruits().get(i).getName());
		}
	}

	@Test
	public void addFruit_ExpectSuccess() {
		FruitDto fruitDto = new FruitDto("fruit");
		FruitDto dto = fruitService.addFruit(fruitDto);
		assertEquals(fruitDto.getName(), dto.getName());
	}

	@Test(expected = FruitAlreadyExistException.class)
	public void addFruit_ExpectException() {
		FruitDto fruitDto = new FruitDto("fruit");
		fruitService.addFruit(fruitDto);
		fruitService.addFruit(fruitDto);
	}

	@Test(expected = FruitNotFoundException.class)
	public void deleteFruit_expectException() {
		fruitService.deleteFruit("test");
	}
}
